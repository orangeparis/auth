package config_test

import (
	"testing"

	"bitbucket.org/orangeparis/auth/config"
	"github.com/spf13/viper"
)

func TestConfig(t *testing.T) {

	config.Load()
	data := viper.AllSettings()
	_ = data

	s := viper.GetString("auth_server.port")
	//println(s)
	if s != "9096" {
		t.Errorf("tmp was incorrect, got: %s, want: %s", s, "9096")
	}

}
