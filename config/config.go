package config

import (
	"fmt"

	"github.com/spf13/viper"
)

type Configuration struct {
	Name string
}

func init() {

	// Env vars
	viper.SetEnvPrefix("auth")

	// config files
	viper.AddConfigPath("/etc/auth/")  // path to look for the config file in
	viper.AddConfigPath("$HOME/.auth") // call multiple times to add many search paths
	viper.AddConfigPath(".")           // optionally look for config in the working directory

	// defaults

	// [auth_server]
	viper.SetDefault("auth_server.port", "9096")

	viper.SetDefault("auth_server.credentials", "/credentials")
	viper.SetDefault("auth_server.token", "/token")

	viper.SetDefault("auth_server.static", "./static")
	viper.SetDefault("auth_server.templates", "./templates")

	// [auth_db]
	viper.SetDefault("auth_db.db", "redis")
	viper.SetDefault("auth_db.redis_host", "localhost")

	// [auth_client]
	viper.SetDefault("auth_client.token_url", "localhost:9096/token")

}

// Load : read the config files
func Load() {

	// read main "config.toml" file
	viper.SetConfigName("config") // name of config file (without extension)
	err := viper.ReadInConfig()   // Find and read the config file
	if err != nil {               // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

}

// Get : Get config key
func Get(key string) interface{} {
	return viper.Get(key)
}

// Set config key with value
func Set(key string, value interface{}) {
	viper.Set(key, value)
}

// GetString : get key as string
func GetString(key string) string {
	return viper.GetString(key)
}
