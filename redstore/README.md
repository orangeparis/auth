# redstore : a redis store for oauth2 token

implements interface from gopkg.in/oauth2.v3

status:  not created




## TokenStore

    // TokenStore the token information storage interface
    TokenStore interface {
        // create and store the new token information
        Create(info TokenInfo) error

        // delete the authorization code
        RemoveByCode(code string) error

        // use the access token to delete the token information
        RemoveByAccess(access string) error

        // use the refresh token to delete the token information
        RemoveByRefresh(refresh string) error

        // use the authorization code for token information data
        GetByCode(code string) (TokenInfo, error)

        // use the access token for token information data
        GetByAccess(access string) (TokenInfo, error)

        // use the refresh token for token information data
        GetByRefresh(refresh string) (TokenInfo, error)
    }

## TokenInfo

    // TokenInfo the token information model interface
    TokenInfo interface {
        New() TokenInfo

        GetClientID() string
        SetClientID(string)
        GetUserID() string
        SetUserID(string)
        GetRedirectURI() string
        SetRedirectURI(string)
        GetScope() string
        SetScope(string)

        GetCode() string
        SetCode(string)
        GetCodeCreateAt() time.Time
        SetCodeCreateAt(time.Time)
        GetCodeExpiresIn() time.Duration
        SetCodeExpiresIn(time.Duration)

        GetAccess() string
        SetAccess(string)
        GetAccessCreateAt() time.Time
        SetAccessCreateAt(time.Time)Ò
        GetAccessExpiresIn() time.Duration
        SetAccessExpiresIn(time.Duration)

        GetRefresh() string
        SetRefresh(string)
        GetRefreshCreateAt() time.Time
        SetRefreshCreateAt(time.Time)
        GetRefreshExpiresIn() time.Duration
        SetRefreshExpiresIn(time.Duration)
    }
